from domainmodel.attributes import AttributeMethodsMixin
from domainmodel.base import dynamics, models

class Person(AttributeMethodsMixin):
    age = models.Field()

    class Meta:
        attr_method_prefixes = [
            dynamics.AttrMethodPrefix('reset')
        ]
        attr_method_suffixes = [
            dynamics.AttrMethodSuffix('_is_highset')
        ]
        define_attr_methods_for = [
            dynamics.AttrMethodFor('age')
        ]

    def reset_attribute(self, attribute):
        setattr(self, attribute, 0)

    def attribute_is_highest(self, attribute):
        return getattr(self, attribute) > 100


person = Person()
person.age = 110
assert person.age_is_highest() is True
person.reset_age()
assert person.age_is_highest() is False
