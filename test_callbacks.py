from domainmodel.callbacks import CallbacksMixin
from domainmodel.base import dynamics, models


class Person(CallbacksMixin):
    class Meta:
        define_callbacks = [
            dynamics.CallbackFor('update')
        ]
        before_callbacks = [
            dynamics.BeforeCallback('update', 'reset_me')
        ]

    def update(self):
        def inner():
            print('inner executed')

    def reset_me(self):
        print('reset_me executed')


person = Person()
person.update()
