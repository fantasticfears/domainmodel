from domainmodel.attributes import AttributeMethodsMixin
from domainmodel.base import dynamics, models

class Person(AttributeAssignmentMixin):
    age = models.Field()


person = Person()
person.assign_attributes({ age: 110 })
print(person.attributes)
