class DomainModelBase(type):
    """Metaclass for all domain models."""
    def __new__(cls, name, bases, attrs):
        super_new = super().__new__

        # Create the class.
        module = attrs.pop('__module__')
        new_attrs = {'__module__': module}
        classcell = attrs.pop('__classcell__', None)
        if classcell is not None:
            new_attrs['__classcell__'] = classcell
        new_class = super_new(cls, name, bases, new_attrs)

        # Add all attributes to the class.
        for obj_name, obj in attrs.items():
            print(obj_name, obj)
            setattr(new_class, obj_name, obj)

        # TODO: consider inheritance
        return new_class
