from domainmodel.base import dynamics, models

class Person(models.Model):
    age = models.Field()
    first_name = models.TrackedField()
    last_name = models.TrackedField()

    class Meta:
        dirty = True
        define_attr_methods_for = [
            dynamics.AttrMethodFor('age'),
            dynamics.AttrMethodFor('first_name')
            dynamics.AttrMethodFor('last_name')
        ]
        define_callbacks = [
            dynamics.CallbackFor('update')
        ]
        before_callbacks = [
            dynamics.BeforeCallback('update', 'reset_me')
        ]
    def reset_attribute(self, attribute):
        setattr(self, attribute, 0)

    def attribute_is_highest(self, attribute):
        return getattr(self, attribute) > 100

    def update(self):
        def inner():
            print('inner executed')

    def reset_me(self):
        print('reset_me executed')

    def save(self):
        # do save work...
        changes_applied()

person = Person({ age: 110 })
person.age = 20
person.update()

assert person.has_changed is False

person.first_name = 'First Name'
assert person.first_name == 'First Name'

assert person.has_changed is True

assert person.changed == ['first_name']

assert person.changed_attributes == {'first_name': None}

assert person.changes == {'first_name': (None, 'First Name')}

assert person.first_name == 'First Name'
assert person.first_name_has_changed is True

assert person.first_name_was is None
