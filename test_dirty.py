from domainmodel.dirty import DirtyMixin
from domainmodel.base import dynamics, models


class Person(DirtyMixin):
    first_name = models.TrackedField()
    last_name = models.TrackedField()
    class Meta:
        dirty = True
        define_attr_methods_for = [
            dynamics.AttrMethodFor('first_name'),
            dynamics.AttrMethodFor('last_name')
        ]

    def save(self):
        # do save work...
        self.changes_applied()


person = Person()
assert person.has_changed is False

person.first_name = 'First Name'
assert person.first_name == 'First Name'

assert person.has_changed is True

assert person.changed == ['first_name']

assert person.changed_attributes == {'first_name': None}

assert person.changes == {'first_name': (None, 'First Name')}

assert person.first_name == 'First Name'
assert person.first_name_has_changed is True

assert person.first_name_was is None

assert person.first_name_change == (None, 'First Name')
assert person.last_name_change is None
