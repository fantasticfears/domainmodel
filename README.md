## 4 modules

- Validation
- Attribute Method
- Callbacks
- Dirty

## Design

### `Meta` class

`Meta` class is present in Django as it's convenient to configure the model
behaviors. The very same functionalities in other languages are done by functional calls, such as ActiveRecord in Ruby on Rails.

For a domain model like we intended to design, it's mostly likely to be useful
in debugging process and in REPL. Users can enjoy access
the attributes dynamically and even with monkey patching. 

More importantly, the package focus on domain logic rather than the persistence.
The logic belongs to the persistence should be in its parent class or in other
services.

### In favor of decoration

Attributes defined in the model, and the Meta information describes how attributes and model is also coherent and seemly. The decoration unnatural
and requires you write the function calls explicitly.

For example,

```python
from xx import db

class Person(db.Model):
    @before('nickname_person')
    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs)
```

Apparently, `save` validates the attributes being saved and how it's persistent,
it's largely irrelevant to make a nickname for that person.

In contrast, the domain model extracts such logic to form a logical set of
functions performed before the `save`.
